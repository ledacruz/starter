package com.mba.starter.model;


import lombok.*;

@Data
@Builder
public class Starter {

    public String eventType;
    public String timestamp;
}

package com.mba.starter.messaging;

import com.google.gson.Gson;
import com.mba.starter.model.Starter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

@Slf4j
@Service
public class StarterProducerCho{
    private static final Logger logger = LoggerFactory.getLogger(StarterProducerCho.class);

    private static KafkaTemplate<String, String> kafkaTemplate;

    public StarterProducerCho(KafkaTemplate<String, String> kafkaTemplate){
        this.kafkaTemplate = kafkaTemplate;
    }

    public static void main(String[] args) {

        String bootstrapServers = "localhost:9092";

        // criar as propriedades do Producer
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // criar o producer
        // <key, value>
        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss.SSS");
        Date date = new Date();

        dateFormat.format(date);

        Gson gson = new Gson();
        Starter starter = Starter.builder().eventType("transfer.received").timestamp(dateFormat.format(date)).build();
        String finalMessage = gson.toJson(starter);

        // criar um producer record
        ProducerRecord<String, String> record = new ProducerRecord<String, String>("choreography-topic", finalMessage);

        // enviar o dado
        producer.send(record);

        // flush data
        producer.flush();

        // flush and close producer
        producer.close();




        logger.info(String.format("$$ -> Producing message --> %s", finalMessage));


    }

}